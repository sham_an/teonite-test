﻿This project was bootstrapped with Create React App and is dockerized with docker-compose.
(https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:


### `docker-compose up -d`
Launches all docker containers. <br>
### `docker-compose stop`
Stops all docker containers <br>
###`docker-compose exec web npm install package`
Installs package using NPM in docker container <br>