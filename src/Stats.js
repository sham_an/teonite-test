import React, { Component } from 'react';
// import Select from 'react-select';
import AsyncSelect from 'react-select/lib/Async';
/*AXIOS*/
import axios from 'axios';
/*RESTSTRAP*/
import { Table, Button } from 'reactstrap';
/*CSS*/
import './stats.css';

class Stats extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedOption: [{ value: "all", label: "All" }],
            statistics: null
        }; // default all option?
    };

    handleChange = (selectedOption) => {
        if (this.isAllOptionSelected(selectedOption) && this.isAllOptionSelected(this.state.selectedOption)) {
            // In new options All is selected and in previous options All is selected - author is added 
            selectedOption = selectedOption.filter((option) => option.value !== 'all');
        } else if (!this.isAllOptionSelected(this.state.selectedOption) && this.isAllOptionSelected(selectedOption)) {
            selectedOption = selectedOption.filter((option) => option.value === 'all');
        }
        this.setState({ selectedOption });
    }

    handleClick = () => {
        // getting values from input after click - Array of objects
        if (this.isAllOptionSelected(this.state.selectedOption)) {
            this.loadAllStatistics();
            console.log("Zawiera opcję All");
        } else if (this.state.selectedOption.length > 0) {
            this.loadAuthorsStatistic();
            console.log("nie zawiera opcji All");
        }
    }

    render() {
        const { selectedOption } = this.state;
        return (
            <div>
                <AsyncSelect className="mt-5"
                    isMulti
                    name="authors"
                    value={selectedOption}
                    onChange={this.handleChange}
                    cacheOptions
                    defaultOptions
                    loadOptions={this.loadAuthorsOptions}
                />
                <div className="row justify-content-center my-5">
                    <Button onClick={this.handleClick}>Submit</Button>
                </div>
                {this.state.statistics &&
                    <Table striped>
                        <thead>
                            <tr>
                                <th></th>
                                <th>Word</th>
                                <th>Count</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.state.statistics.map((stat, index) =>
                                <tr>
                                    <td>{index + 1}</td>
                                    <td>{stat.word}</td>
                                    <td>{stat.count}</td>
                                </tr>)}
                        </tbody>
                    </Table>}
            </div>
        );
    }

    loadAuthorsStatistic = () => {
        let requests = [];

        this.state.selectedOption.forEach((option) => {
            requests.push(axios.get(`http://localhost:8080/stats/${option.value}`));
        });
        axios.all(requests).then((responses) => {
            let statistics = [];
            let counter = {};
            responses.forEach((response) => {
                Object.keys(response.data).forEach(key => {
                    // statistics.push({ word: key, count: response.data[key] });
                    if (key in counter) {
                        //word used in obj - adding it
                        counter[key] += response.data[key];
                    } else {
                        counter[key] = response.data[key];
                    }
                });
            });
            Object.keys(counter).forEach(key => {
                statistics.push({ word: key, count: counter[key] });
            });
            this.setState({ statistics });
        });
    }

    loadAuthorsOptions = () => {
        return axios.get(`http://localhost:8080/authors/`)
            .then(response => {
                let authors = [{ value: 'all', label: 'All' }];
                Object.keys(response.data).forEach(key => {
                    authors.push({ value: key, label: response.data[key] });
                    // console.log(authors);
                });
                return authors;
            });
    }

    loadAllStatistics = () => {
        axios.get(`http://localhost:8080/stats/`)
            .then(response => {
                let statistics = [];
                Object.keys(response.data).forEach(key => {
                    statistics.push({ word: key, count: response.data[key] });
                });
                this.setState({ statistics });
            });
    }

    isAllOptionSelected = (selectedOptions) => {
        return selectedOptions.some(option => option.value === 'all');
    }

}

export default Stats;