import React, { Component, Fragment } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import './Home.css';

class Home extends Component {
    render() {
        return (
            <Fragment>
                <div className="home-main-container">
                <FontAwesomeIcon icon="home" />
                <h1 className="home-header">Strona główna</h1>
                </div>
            </Fragment>
        );
    }
}

export default Home;