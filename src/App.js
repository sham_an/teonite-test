import React, { Component, Fragment } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
/*MAIN-PAGES*/
import Header from './Header';
import Stats from './Stats';
import Home from './Home';
/*BOOTSTRAP*/
import NotFound from './NotFound';
import { Container } from 'reactstrap';
/*FONT-AWESOME:*/
import { library } from '@fortawesome/fontawesome-svg-core'
import { faTh, faHome } from '@fortawesome/free-solid-svg-icons'

class App extends Component {
  render() {
    return (
        <Router>
          <Fragment>
            <Header />
            <Container>
              <Switch>
                <Route exact path="/" component={Home} />
                <Route path="/stats" component={Stats} />
                <Route component={NotFound} />
              </Switch>
            </Container>
          </Fragment>
        </Router>
    );
  }
}
library.add(faTh, faHome)
export default App;
