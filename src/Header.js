import React, { Component, Fragment } from 'react';
import { Navbar, Nav, NavItem } from 'reactstrap';
import { Link, NavLink } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import './Header.css';

class Header extends Component {
  render() {
    return (
      <Fragment>
        <Navbar color="light" light expand="md" sticky="top">
          <Link to="/" className="navbar-brand">
          <FontAwesomeIcon icon="th" color="#426B69"/>
          </Link>
          <Nav pills>
            <NavItem>
              <NavLink exact to="/" className="nav-link">
                Home
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink to="/stats" className="nav-link">
                Stats
              </NavLink>
            </NavItem>
          </Nav>
        </Navbar>
      </Fragment>
    );
  }
}

export default Header;